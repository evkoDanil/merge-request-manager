package com.mr.manager.merge_request_manager_bot.enums;

import lombok.Getter;

@Getter
public enum BotState {

    DEFAULT("НАЧАЛЬНОЕ СОСТОЯНИЕ"),
    ADD_USER_LOGIN("Добавление логина нового пользователя"),
    ADD_USER_ROLE("Добавление роли нового пользователя"),
    DELETE_MEMBER("Удаление пользователя"),
    EDIT_ROLE("Редактирование роли");

    private final String name;

    BotState(String name) {
        this.name = name;
    }
}
