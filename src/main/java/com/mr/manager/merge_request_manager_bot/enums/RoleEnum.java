package com.mr.manager.merge_request_manager_bot.enums;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public enum RoleEnum {

    ADMIN(1, "admin"),
    REVIEWER(2, "reviewer"),
    MEMBER(3, "member");

    private final int code;
    private final String name;

    RoleEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
