package com.mr.manager.merge_request_manager_bot.dto;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ServiceMessageConstants {

    public static final String NOT_ENOUGH_RIGHTS_TO_ADD_MEMBER = "Недотаточно прав для добавления пользователя";
    public static final String MEMBER_ADDED_SUCCESSFULLY = "Пользовательasdasdqweqwe добавлен успешно!";
    public static final String NO_COMMAND_RECOGNIZED = "No command Recognized";
    public static final String CHOOSE_MEMBER_ROLE = "Выберите роль пользователя";
    public static final String BEFORE_ADD_NEW_END_ADD_PREVIOUS = "Перед добавлением нового пользоавтеля следует завершить добавление предыдущего";
    public static final String WRITE_NEW_MEMBER_LOGIN = "Напишите логин пользователя, которого хотите добавить";
    public static final String USER_NOT_FOUND = "Пользователь не найден!";
    public static final String USER_ALREADY_EXISTS = "Пользователь с таким логином уже зарегистрирован!";
    public static final String USER_DOES_NOT_EXISTS = "Пользователя с таким логином не существует";
    public static final String USER_DELETED_SUCCESSFULLY = "Пользователь удален";
    public static final String NO_CALLBACKS_FOUND = "Нет колбэков";
    public static final String ACTION_CANCELED = "Действие отменено";
    public static final String ALREADY_CANCELED = "Действие уже отменено";
    public static final String WRITE_MEMBER_TO_DELETE_LOGIN = "Напишите логин пользователя, которого хотите удалить";
    public static final String END_PREVIOUS_ACTIVITY = "Завершите предыдущую активность";
    public static final String WRITE_MEMBER_LOGIN_TO_EDIT_ROLE = "Ведите логин пользователя для изменения роли";
}
