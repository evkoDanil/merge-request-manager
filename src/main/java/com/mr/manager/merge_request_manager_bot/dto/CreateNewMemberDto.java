package com.mr.manager.merge_request_manager_bot.dto;

import com.mr.manager.merge_request_manager_bot.enums.BotState;
import com.mr.manager.merge_request_manager_bot.model.Role;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class CreateNewMemberDto {

    private String loginTg;
    private Long chatId;
    private Role role;
    private BotState botState;
}
