package com.mr.manager.merge_request_manager_bot.config;

import com.mr.manager.merge_request_manager_bot.service.CallbackProcessor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@RequiredArgsConstructor
@Data
public class CallbackProcessorConfig {
    private final List<CallbackProcessor> callbackProcessors;
}
