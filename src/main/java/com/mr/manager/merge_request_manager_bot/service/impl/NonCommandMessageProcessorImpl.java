package com.mr.manager.merge_request_manager_bot.service.impl;

import com.mr.manager.merge_request_manager_bot.dto.CreateNewMemberDto;
import com.mr.manager.merge_request_manager_bot.dto.ServiceMessageConstants;
import com.mr.manager.merge_request_manager_bot.dto.UpdateUserStateDto;
import com.mr.manager.merge_request_manager_bot.enums.RoleEnum;
import com.mr.manager.merge_request_manager_bot.model.TeamMember;
import com.mr.manager.merge_request_manager_bot.service.MessageService;
import com.mr.manager.merge_request_manager_bot.service.NonCommandMessageProcessor;
import com.mr.manager.merge_request_manager_bot.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class NonCommandMessageProcessorImpl implements NonCommandMessageProcessor {

    private final UserService userService;
    private final MessageService messageService;

    public static final String REVIEWER = "reviewer";
    public static final String ADMIN = "admin";
    public static final String MEMBER = "member";
    public static final String CANCEL = "Отмена";

    public SendMessage process(Message message) {
        TeamMember member = userService.getByLoginTg(message.getChat().getUserName());
        if (member == null) {
            log.error("Пользоватеqweqweль не найден!");
            return messageService.sendMessage(message.getChatId(), ServiceMessageConstants.USER_NOT_FOUND);
        } else {
            switch (member.getCurrentBotState()) {
                case ADD_USER_LOGIN:
                    return createNewMember(member, message);
                case DELETE_MEMBER:
                    return deleteMember(member, message);
                case EDIT_ROLE:
                    return editMemberRole(member, message);
                default:
                    return messageService.sendMessage(message.getChatId(), ServiceMessageConstants.NO_COMMAND_RECOGNIZED);
            }
        }
    }

    private SendMessage editMemberRole(TeamMember admin, Message message) {
        TeamMember memberToEdit = userService.getByLoginTg(message.getText());
        if (memberToEdit == null) {
            log.error("Пользователь не найден");
            return messageService.sendMessage(message.getChatId(), ServiceMessageConstants.USER_NOT_FOUND);
        } else {
            UpdateUserStateDto updateUserStateDto = UpdateUserStateDto.builder()
                    .newMemberId(memberToEdit.getId())
                    .build();
            userService.updateUserState(admin, updateUserStateDto);

            return sendMessageToChooseRole(admin);
        }
    }

    private SendMessage createNewMember(TeamMember admin, Message message) {
        if (userService.getByLoginTg(message.getText()) != null) {
            log.error("Пользователь с таким логином уже зарегистрирован!");
            return messageService.sendMessage(message.getChatId(), ServiceMessageConstants.USER_ALREADY_EXISTS);
        } else {
            CreateNewMemberDto createNewMemberDto = CreateNewMemberDto.builder()
                    .loginTg(message.getText())
                    .build();
            Long newMemberId = userService.createNewMember(createNewMemberDto);

            UpdateUserStateDto updateUserStateDto = UpdateUserStateDto.builder()
                    .newMemberId(newMemberId)
                    .build();
            userService.updateUserState(admin, updateUserStateDto);
            return sendMessageToChooseRole(admin);
        }
    }

    private SendMessage deleteMember(TeamMember member, Message message) {
        TeamMember memberToDelete = userService.getByLoginTg(message.getText());

        if (memberToDelete == null) {
            return messageService.sendMessage(message.getChatId(), ServiceMessageConstants.USER_DOES_NOT_EXISTS);
        } else {
            userService.deleteMember(memberToDelete);
            userService.setToDefault(member);
            return messageService.sendMessage(member.getChatId(), ServiceMessageConstants.USER_DELETED_SUCCESSFULLY);
        }
    }

    private SendMessage sendMessageToChooseRole(TeamMember member) {
        SendMessage sendMessage = messageService.sendMessage(member.getChatId(),
                ServiceMessageConstants.CHOOSE_MEMBER_ROLE);
        sendMessage.setReplyMarkup(buildInlineMarkupChooseRole());
        return sendMessage;
    }

    private InlineKeyboardMarkup buildInlineMarkupChooseRole() {
        InlineKeyboardMarkup inlineMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
        List<InlineKeyboardButton> rowInline = new ArrayList<>();

        var adminRoleButton = createButton(ADMIN, ADMIN);
        var memberRoleButton = createButton(MEMBER, MEMBER);
        var reviewerRoleButton = createButton(REVIEWER, REVIEWER);
        var cancelButton = createButton(CANCEL, CANCEL);

        rowInline.add(adminRoleButton);
        rowInline.add(memberRoleButton);
        rowInline.add(reviewerRoleButton);
        rowInline.add(cancelButton);

        rowsInline.add(rowInline);
        inlineMarkup.setKeyboard(rowsInline);

        return inlineMarkup;
    }

    private InlineKeyboardButton createButton(String text, String callbackData) {
        var inlineButton = new InlineKeyboardButton();
        inlineButton.setText(text);
        inlineButton.setCallbackData(callbackData);

        return inlineButton;
    }
}
