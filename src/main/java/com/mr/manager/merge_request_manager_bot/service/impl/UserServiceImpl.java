package com.mr.manager.merge_request_manager_bot.service.impl;

import com.mr.manager.merge_request_manager_bot.dto.CreateNewMemberDto;
import com.mr.manager.merge_request_manager_bot.dto.UpdateUserStateDto;
import com.mr.manager.merge_request_manager_bot.enums.BotState;
import com.mr.manager.merge_request_manager_bot.model.Role;
import com.mr.manager.merge_request_manager_bot.model.TeamMember;
import com.mr.manager.merge_request_manager_bot.repository.TeamMemberRepository;
import com.mr.manager.merge_request_manager_bot.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final TeamMemberRepository teamMemberRepository;

    private static final Integer ADMIN_ROLE_CODE = 1;

    public TeamMember getByLoginTg(String loginTg) {
        return teamMemberRepository.findByLoginTg(loginTg)
                .orElse(null);
    }

    public Long createNewMember(CreateNewMemberDto createNewMemberDto) {
        TeamMember newMember = new TeamMember();

        newMember.setCurrentBotState(BotState.DEFAULT);
        newMember.setChatId(createNewMemberDto.getChatId());
        newMember.setLoginTg(createNewMemberDto.getLoginTg());

        return teamMemberRepository.save(newMember).getId();
    }

    public void updateUserState(TeamMember member, UpdateUserStateDto dto) {
        if (dto.getBotState() != null) {
            member.setCurrentBotState(dto.getBotState());
        }
        if (dto.getNewMemberId() != null) {
            member.setNewMemberId(dto.getNewMemberId());
        }
        if (dto.getRole() != null) {
            member.setRole(dto.getRole());
        }
        teamMemberRepository.save(member);
    }

    @Override
    public void setChatId(TeamMember member, Long chatId) {
        member.setChatId(chatId);
        teamMemberRepository.save(member);
    }

    @Override
    public TeamMember getById(Long id) {
        return teamMemberRepository.findById(id)
                .orElse(null);
    }

    public List<TeamMember> getAllMembers() {
        return teamMemberRepository.findAll();
    }

    public void deleteMember(TeamMember teamMember) {
        teamMemberRepository.delete(teamMember);
    }

    public boolean isAdmin(String userName) {
        Role role = teamMemberRepository.findByLoginTg(userName)
                .map(TeamMember::getRole)
                .orElse(null);
        if (role != null) {
            return role.getCode() == ADMIN_ROLE_CODE;
        }
        return false;
    }

    public void setToDefault(TeamMember member) {
        UpdateUserStateDto updateUserStateDto = UpdateUserStateDto.builder()
                .newMemberId(0L)
                .botState(BotState.DEFAULT)
                .build();
        updateUserState(member, updateUserStateDto);
    }
}
