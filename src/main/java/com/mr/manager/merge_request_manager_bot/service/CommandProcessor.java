package com.mr.manager.merge_request_manager_bot.service;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

public interface CommandProcessor {

    /**
     * Возвращает наименование обрабатываемой команды
     *
     * @return команда, например /start
     */
    String getCommandName();

    /**
     * Обработка команды
     *
     * @param chatId  - айди чата
     * @param message - объект {@link Message}
     */
    SendMessage process(Long chatId, Message message);
}
