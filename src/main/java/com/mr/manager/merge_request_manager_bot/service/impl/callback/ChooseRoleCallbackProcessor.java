package com.mr.manager.merge_request_manager_bot.service.impl.callback;

import com.mr.manager.merge_request_manager_bot.dto.ServiceMessageConstants;
import com.mr.manager.merge_request_manager_bot.dto.UpdateUserStateDto;
import com.mr.manager.merge_request_manager_bot.enums.BotState;
import com.mr.manager.merge_request_manager_bot.enums.RoleEnum;
import com.mr.manager.merge_request_manager_bot.model.Role;
import com.mr.manager.merge_request_manager_bot.model.TeamMember;
import com.mr.manager.merge_request_manager_bot.repository.RoleRepository;
import com.mr.manager.merge_request_manager_bot.service.CallbackProcessor;
import com.mr.manager.merge_request_manager_bot.service.MessageService;
import com.mr.manager.merge_request_manager_bot.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ChooseRoleCallbackProcessor implements CallbackProcessor {

    private final UserService userService;
    private final RoleRepository roleRepository;
    private final MessageService messageService;

    public static final String REVIEWER = "reviewer";
    public static final String ADMIN = "admin";
    public static final String MEMBER = "member";

    private static final List<String> PROCESSING_CALLBACKS = List.of(ADMIN, MEMBER, REVIEWER);

    @Override
    public List<String> getProcessingCallbacks() {
        return PROCESSING_CALLBACKS;
    }

    @Override
    public SendMessage process(CallbackQuery callbackQuery) {
        String userName = callbackQuery.getMessage().getChat().getUserName();
        switch (callbackQuery.getData()) {
            case REVIEWER:
                return setUserRole(userName, RoleEnum.REVIEWER);
            case ADMIN:
                return setUserRole(userName, RoleEnum.ADMIN);
            case MEMBER:
                return setUserRole(userName, RoleEnum.MEMBER);
            default:
                return messageService.sendMessage(callbackQuery.getMessage().getChatId(), ServiceMessageConstants.NO_COMMAND_RECOGNIZED);
        }
    }

    private SendMessage setUserRole(String memberLogin, RoleEnum role) {
        TeamMember admin = userService.getByLoginTg(memberLogin);

        if (admin.getNewMemberId() != 0) {
            TeamMember newMember = userService.getById(admin.getNewMemberId());

            Role choosenRole = roleRepository.findByName(role)
                    .orElse(null);

            UpdateUserStateDto updateUserStateDto = UpdateUserStateDto.builder()
                    .role(choosenRole)
                    .build();

            userService.updateUserState(newMember, updateUserStateDto);
            userService.setToDefault(admin);

            return messageService.sendMessage(admin.getChatId(), ServiceMessageConstants.MEMBER_ADDED_SUCCESSFULLY);
        } else {
            return messageService.sendMessage(admin.getChatId(), ServiceMessageConstants.BEFORE_ADD_NEW_END_ADD_PREVIOUS);
        }
    }
}
