package com.mr.manager.merge_request_manager_bot.service.impl;

import com.mr.manager.merge_request_manager_bot.model.TeamMember;
import com.mr.manager.merge_request_manager_bot.service.MessageService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

@RequiredArgsConstructor
@Service
public class MessageServiceImpl implements MessageService {

    private static final String HELLO_MESSAGE = "Приветсвтую тебя, @%s, ты зашел в бота управленя мердж реквестами." +
            "\n Твоя роль: %s";

    public SendMessage sendHelloMessage(Long chatId, TeamMember teamMember) {

        return sendMessage(chatId, String.format(
                HELLO_MESSAGE,
                teamMember.getLoginTg(),
                teamMember.getRole().getName().getName()));
    }

    @Override
    public SendMessage sendMessageWithInline() {
        return null;
    }

    @Override
    public SendMessage sendMessage(Long chatId, String textToSend) {
        SendMessage message = new SendMessage();
        message.setChatId(chatId);
        message.setText(textToSend);
        return message;
    }


}
