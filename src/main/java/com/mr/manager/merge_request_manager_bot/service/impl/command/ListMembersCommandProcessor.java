package com.mr.manager.merge_request_manager_bot.service.impl.command;

import com.mr.manager.merge_request_manager_bot.model.TeamMember;
import com.mr.manager.merge_request_manager_bot.service.CommandProcessor;
import com.mr.manager.merge_request_manager_bot.service.MessageService;
import com.mr.manager.merge_request_manager_bot.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ListMembersCommandProcessor implements CommandProcessor {

    private final UserService userService;
    private final MessageService messageService;

    public static final String LIST_MEMBERS = "/listmembers";

    @Override
    public String getCommandName() {
        return LIST_MEMBERS;
    }

    @Override
    public SendMessage process(Long chatId, Message message) {
        List<TeamMember> members = userService.getAllMembers();
        StringBuilder responseMessage = new StringBuilder();

        members.forEach(
                member -> responseMessage
                        .append(member.getLoginTg())
                        .append(" роль: ")
                        .append(member.getRole().getName().getName())
                        .append("\n")
        );
        return messageService.sendMessage(chatId, responseMessage.toString());
    }
}
