package com.mr.manager.merge_request_manager_bot.service.impl.command;

import com.mr.manager.merge_request_manager_bot.dto.ServiceMessageConstants;
import com.mr.manager.merge_request_manager_bot.dto.UpdateUserStateDto;
import com.mr.manager.merge_request_manager_bot.enums.BotState;
import com.mr.manager.merge_request_manager_bot.model.TeamMember;
import com.mr.manager.merge_request_manager_bot.service.CommandProcessor;
import com.mr.manager.merge_request_manager_bot.service.MessageService;
import com.mr.manager.merge_request_manager_bot.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

@Service
@RequiredArgsConstructor
@Slf4j
public class AddUserCommandProcessor implements CommandProcessor {

    private final UserService userService;
    private final MessageService messageService;

    public static final String ADD_USER = "/adduser";

    @Override
    public String getCommandName() {
        return ADD_USER;
    }

    @Override
    public SendMessage process(Long chatId, Message message) {
        if (userService.isAdmin(message.getChat().getUserName())) {
            return startAddingNewMember(userService.getByLoginTg(message.getChat().getUserName()), message);
        } else {
            return messageService.sendMessage(message.getChatId(),
                    ServiceMessageConstants.NOT_ENOUGH_RIGHTS_TO_ADD_MEMBER);
        }
    }

    private SendMessage startAddingNewMember(TeamMember adminMember, Message message) {
        if (adminMember.getCurrentBotState() != BotState.DEFAULT) {
            return messageService.sendMessage(message.getChatId(),
                    ServiceMessageConstants.BEFORE_ADD_NEW_END_ADD_PREVIOUS);
        } else {
            UpdateUserStateDto updateUserStateDto = UpdateUserStateDto.builder()
                    .botState(BotState.ADD_USER_LOGIN)
                    .build();
            userService.updateUserState(adminMember, updateUserStateDto);

            return messageService.sendMessage(message.getChatId(),
                    ServiceMessageConstants.WRITE_NEW_MEMBER_LOGIN);
        }
    }
}
