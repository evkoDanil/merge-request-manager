package com.mr.manager.merge_request_manager_bot.service;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;

import java.util.List;

public interface CallbackProcessor {

    List<String> getProcessingCallbacks();

    SendMessage process(CallbackQuery callbackQuery);
}
