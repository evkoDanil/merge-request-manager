package com.mr.manager.merge_request_manager_bot.service.impl.callback;

import com.mr.manager.merge_request_manager_bot.dto.ServiceMessageConstants;
import com.mr.manager.merge_request_manager_bot.dto.UpdateUserStateDto;
import com.mr.manager.merge_request_manager_bot.enums.BotState;
import com.mr.manager.merge_request_manager_bot.enums.RoleEnum;
import com.mr.manager.merge_request_manager_bot.model.TeamMember;
import com.mr.manager.merge_request_manager_bot.service.CallbackProcessor;
import com.mr.manager.merge_request_manager_bot.service.MessageService;
import com.mr.manager.merge_request_manager_bot.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CancelActionCallbackProcessor implements CallbackProcessor {

    private final UserService userService;
    private final MessageService messageService;

    private static final String CANCEL = "Отмена";
    private static final List<String> CANCEL_CALLBACK = List.of(CANCEL);

    @Override
    public List<String> getProcessingCallbacks() {
        return CANCEL_CALLBACK;
    }

    @Override
    public SendMessage process(CallbackQuery callbackQuery) {
        TeamMember member = userService.getByLoginTg(callbackQuery.getMessage().getChat().getUserName());

        switch (member.getCurrentBotState()) {
            case ADD_USER_LOGIN -> {
                return cancelCreatingNewMember(member, callbackQuery);
            }
            case DELETE_MEMBER -> {
                return cancelDeletingMember(member, callbackQuery);
            }
            default -> {
                return alreadyCanceledMessage(member, callbackQuery);
            }
        }
    }

    private SendMessage cancelCreatingNewMember(TeamMember member, CallbackQuery callbackQuery) {
        TeamMember memberToDelete = userService.getById(member.getNewMemberId());
        if (memberToDelete != null) {
            userService.deleteMember(userService.getById(member.getNewMemberId()));
            userService.setToDefault(member);

            return messageService.sendMessage(callbackQuery.getMessage().getChatId(), ServiceMessageConstants.ACTION_CANCELED);
        } else {
            return alreadyCanceledMessage(member, callbackQuery);
        }
    }

    private SendMessage alreadyCanceledMessage(TeamMember member, CallbackQuery callbackQuery) {
        userService.setToDefault(member);
        return messageService.sendMessage(callbackQuery.getMessage().getChatId(), ServiceMessageConstants.ALREADY_CANCELED);
    }

    private SendMessage cancelDeletingMember(TeamMember member, CallbackQuery callbackQuery) {
        userService.setToDefault(member);
        return messageService.sendMessage(callbackQuery.getMessage().getChatId(), ServiceMessageConstants.ACTION_CANCELED);
    }
}
