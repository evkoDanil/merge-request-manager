package com.mr.manager.merge_request_manager_bot.service;

import com.mr.manager.merge_request_manager_bot.dto.CreateNewMemberDto;
import com.mr.manager.merge_request_manager_bot.dto.UpdateUserStateDto;
import com.mr.manager.merge_request_manager_bot.model.TeamMember;

import java.util.List;

public interface UserService {
    TeamMember getByLoginTg(String loginTg);

    Long createNewMember(CreateNewMemberDto createNewMemberDto);

    boolean isAdmin(String userName);

    void updateUserState(TeamMember member, UpdateUserStateDto dto);

    void setChatId(TeamMember member, Long chatId);

    TeamMember getById(Long id);

    List<TeamMember> getAllMembers();

    void deleteMember(TeamMember teamMember);

    void setToDefault(TeamMember member);
}
