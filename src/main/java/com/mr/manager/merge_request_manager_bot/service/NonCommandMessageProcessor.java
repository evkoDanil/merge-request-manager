package com.mr.manager.merge_request_manager_bot.service;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

public interface NonCommandMessageProcessor {
    SendMessage process(Message message);
}
