package com.mr.manager.merge_request_manager_bot.service.impl.command;

import com.mr.manager.merge_request_manager_bot.dto.CreateNewMemberDto;
import com.mr.manager.merge_request_manager_bot.model.TeamMember;
import com.mr.manager.merge_request_manager_bot.service.CommandProcessor;
import com.mr.manager.merge_request_manager_bot.service.MessageService;
import com.mr.manager.merge_request_manager_bot.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

@Component
@RequiredArgsConstructor
public class StartCommandProcessorImpl implements CommandProcessor {

    private static final String START = "/start";

    private final UserService userService;
    private final MessageService messageService;

    @Override
    public String getCommandName() {
        return START;
    }

    @Override
    public SendMessage process(Long chatId, Message message) {
        TeamMember member = userService.getByLoginTg(message.getChat().getUserName());
        if (member == null) {
            CreateNewMemberDto createNewMemberDto = CreateNewMemberDto.builder()
                    .chatId(message.getChatId())
                    .loginTg(message.getChat().getUserName())
                    .build();
            userService.createNewMember(createNewMemberDto);
        } else if (member.getChatId() == null) {
            userService.setChatId(member, chatId);
        }
        return messageService.sendHelloMessage(chatId, member);
    }
}
