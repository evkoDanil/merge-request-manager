package com.mr.manager.merge_request_manager_bot.service;

import com.mr.manager.merge_request_manager_bot.model.Role;
import com.mr.manager.merge_request_manager_bot.model.TeamMember;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

public interface MessageService {
    SendMessage sendMessage(Long chatId, String textToSend);

    SendMessage sendHelloMessage(Long chatId, TeamMember member);

    SendMessage sendMessageWithInline();
}
