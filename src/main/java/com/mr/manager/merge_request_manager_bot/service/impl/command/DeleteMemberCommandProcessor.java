package com.mr.manager.merge_request_manager_bot.service.impl.command;

import com.mr.manager.merge_request_manager_bot.dto.ServiceMessageConstants;
import com.mr.manager.merge_request_manager_bot.dto.UpdateUserStateDto;
import com.mr.manager.merge_request_manager_bot.enums.BotState;
import com.mr.manager.merge_request_manager_bot.model.TeamMember;
import com.mr.manager.merge_request_manager_bot.service.CommandProcessor;
import com.mr.manager.merge_request_manager_bot.service.MessageService;
import com.mr.manager.merge_request_manager_bot.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DeleteMemberCommandProcessor implements CommandProcessor {

    private final UserService userService;
    private final MessageService messageService;

    public static final String DELETE_MEMBER = "/deletemember";
    public static final String CANCEL = "Отмена";

    @Override
    public String getCommandName() {
        return DELETE_MEMBER;
    }

    @Override
    public SendMessage process(Long chatId, Message message) {
        if (userService.isAdmin(message.getChat().getUserName())) {
            return startDeletingMember(userService.getByLoginTg(message.getChat().getUserName()), message);
        } else {
            return messageService.sendMessage(message.getChatId(),
                    ServiceMessageConstants.NOT_ENOUGH_RIGHTS_TO_ADD_MEMBER);
        }
    }

    private SendMessage startDeletingMember(TeamMember adminMember, Message message) {
        UpdateUserStateDto updateUserStateDto = UpdateUserStateDto.builder()
                .botState(BotState.DELETE_MEMBER)
                .build();
        userService.updateUserState(adminMember, updateUserStateDto);

        SendMessage sendMessage = messageService.sendMessage(message.getChatId(),
                ServiceMessageConstants.WRITE_MEMBER_TO_DELETE_LOGIN);
        sendMessage.setReplyMarkup(buildInlineMarkupCancelDeleting());

        return sendMessage;
    }

    private InlineKeyboardMarkup buildInlineMarkupCancelDeleting() {
        InlineKeyboardMarkup inlineMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
        List<InlineKeyboardButton> rowInline = new ArrayList<>();

        var cancelButton = createButton(CANCEL, CANCEL);

        rowInline.add(cancelButton);

        rowsInline.add(rowInline);
        inlineMarkup.setKeyboard(rowsInline);

        return inlineMarkup;
    }

    private InlineKeyboardButton createButton(String text, String callbackData) {
        var inlineButton = new InlineKeyboardButton();
        inlineButton.setText(text);
        inlineButton.setCallbackData(callbackData);

        return inlineButton;
    }
}
