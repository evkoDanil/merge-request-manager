package com.mr.manager.merge_request_manager_bot.service.impl.command;

import com.mr.manager.merge_request_manager_bot.dto.ServiceMessageConstants;
import com.mr.manager.merge_request_manager_bot.dto.UpdateUserStateDto;
import com.mr.manager.merge_request_manager_bot.enums.BotState;
import com.mr.manager.merge_request_manager_bot.model.TeamMember;
import com.mr.manager.merge_request_manager_bot.service.CommandProcessor;
import com.mr.manager.merge_request_manager_bot.service.MessageService;
import com.mr.manager.merge_request_manager_bot.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

@Service
@RequiredArgsConstructor
public class EditRoleCommandProcessor implements CommandProcessor {

    private final UserService userService;
    private final MessageService messageService;

    public static final String EDIT_ROLE = "/editrole";

    @Override
    public String getCommandName() {
        return EDIT_ROLE;
    }

    @Override
    public SendMessage process(Long chatId, Message message) {
        if (userService.isAdmin(message.getChat().getUserName())) {
            return startEditingRole(userService.getByLoginTg(message.getChat().getUserName()), message);
        } else {
            return messageService.sendMessage(message.getChatId(),
                    ServiceMessageConstants.NOT_ENOUGH_RIGHTS_TO_ADD_MEMBER);
        }
    }

    private SendMessage startEditingRole(TeamMember adminMember, Message message) {
        if (adminMember.getCurrentBotState() != BotState.DEFAULT) {
            return messageService.sendMessage(message.getChatId(),
                    ServiceMessageConstants.END_PREVIOUS_ACTIVITY);
        } else {
            UpdateUserStateDto updateUserStateDto = UpdateUserStateDto.builder()
                    .botState(BotState.EDIT_ROLE)
                    .build();
            userService.updateUserState(adminMember, updateUserStateDto);

            return messageService.sendMessage(message.getChatId(),
                    ServiceMessageConstants.WRITE_MEMBER_LOGIN_TO_EDIT_ROLE);
        }
    }
}
