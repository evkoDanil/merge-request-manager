package com.mr.manager.merge_request_manager_bot.model;

import com.mr.manager.merge_request_manager_bot.enums.BotState;
import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Entity
@Table(name = "team_member")
@Data
public class TeamMember {

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne()
    @JoinColumn(name = "role_id")
    private Role role;

    @Column(name = "login_tg", nullable = false)
    private String loginTg;

    @Column(name = "chat_id")
    private Long chatId;

    @Column(name = "bot_state")
    @Enumerated(EnumType.STRING)
    private BotState currentBotState;

    @Column(name = "newMemberId")
    private Long newMemberId;
}
