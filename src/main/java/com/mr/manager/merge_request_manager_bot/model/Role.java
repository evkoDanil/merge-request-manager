package com.mr.manager.merge_request_manager_bot.model;

import com.mr.manager.merge_request_manager_bot.enums.RoleEnum;
import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Data
@Entity
@Table(name = "role")
public class Role {

    @Id
    @Column(name = "code")
    private int code;

    @Column(name = "name")
    @Enumerated(EnumType.STRING)
    private RoleEnum name;

    @OneToMany(orphanRemoval = true, mappedBy = "role", cascade = CascadeType.ALL)
    private List<TeamMember> teamMember;
}

