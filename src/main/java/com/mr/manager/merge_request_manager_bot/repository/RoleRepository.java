package com.mr.manager.merge_request_manager_bot.repository;

import com.mr.manager.merge_request_manager_bot.enums.RoleEnum;
import com.mr.manager.merge_request_manager_bot.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {

    Optional<Role> findByName(RoleEnum name);
}
