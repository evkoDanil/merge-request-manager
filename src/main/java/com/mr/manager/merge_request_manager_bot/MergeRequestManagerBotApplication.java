package com.mr.manager.merge_request_manager_bot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;

@SpringBootApplication
public class MergeRequestManagerBotApplication {

    public static void main(String[] args) {
        SpringApplication.run(MergeRequestManagerBotApplication.class, args);
    }
}
