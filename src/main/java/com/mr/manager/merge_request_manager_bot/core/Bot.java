package com.mr.manager.merge_request_manager_bot.core;

import com.mr.manager.merge_request_manager_bot.config.BotConfig;
import com.mr.manager.merge_request_manager_bot.config.CallbackProcessorConfig;
import com.mr.manager.merge_request_manager_bot.config.CommandProcessorConfig;
import com.mr.manager.merge_request_manager_bot.dto.ServiceMessageConstants;
import com.mr.manager.merge_request_manager_bot.enums.BotState;
import com.mr.manager.merge_request_manager_bot.enums.RoleEnum;
import com.mr.manager.merge_request_manager_bot.model.Role;
import com.mr.manager.merge_request_manager_bot.model.TeamMember;
import com.mr.manager.merge_request_manager_bot.repository.RoleRepository;
import com.mr.manager.merge_request_manager_bot.repository.TeamMemberRepository;
import com.mr.manager.merge_request_manager_bot.service.CallbackProcessor;
import com.mr.manager.merge_request_manager_bot.service.CommandProcessor;
import com.mr.manager.merge_request_manager_bot.service.NonCommandMessageProcessor;
import com.mr.manager.merge_request_manager_bot.service.UserService;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.commands.SetMyCommands;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.commands.BotCommand;
import org.telegram.telegrambots.meta.api.objects.commands.scope.BotCommandScopeDefault;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
@RequiredArgsConstructor
public class Bot extends TelegramLongPollingBot {

    private final BotConfig botConfig;
    private final RoleRepository roleRepository;
    private final TeamMemberRepository teamMemberRepository;
    private final CommandProcessorConfig commandProcessorConfig;
    private final CallbackProcessorConfig callbackProcessorConfig;
    private final NonCommandMessageProcessor nonCommandMessageProcessor;

    private final List<BotCommand> botCommands = List.of(
            new BotCommand("/adduser", "добавить члена команды"),
            new BotCommand("/start", "start"),
            new BotCommand("/listmembers", "показать членов команды"),
            new BotCommand("/deletemember", "удалить члена команды"),
            new BotCommand("/editrole", "редактировать роль пользователя"));

    @PostConstruct
    public void init() {
        try {
            this.execute(new SetMyCommands(botCommands, new BotCommandScopeDefault(), null));
            log.info("Меню бота сформировано успешно");
            List<Role> roles = new ArrayList<>();
            Role adminRole = new Role();
            adminRole.setCode(1);
            adminRole.setName(RoleEnum.ADMIN);

            Role memberRole = new Role();
            memberRole.setCode(3);
            memberRole.setName(RoleEnum.MEMBER);

            Role reviewerRole = new Role();
            reviewerRole.setCode(2);
            reviewerRole.setName(RoleEnum.REVIEWER);

            roles.add(adminRole);
            roles.add(memberRole);
            roles.add(reviewerRole);
            roleRepository.saveAll(roles);

            TeamMember adminMember = new TeamMember();
            adminMember.setLoginTg("d_evko");
            adminMember.setRole(adminRole);
            adminMember.setCurrentBotState(BotState.DEFAULT);
            teamMemberRepository.save(adminMember);
        } catch (TelegramApiException ex) {
            log.error("Ошибка формирования меню бота\n" + ex.getMessage());
        }
    }

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {
            processMessage(update.getMessage());
        } else if (update.hasCallbackQuery()) {
            processCallbackQuery(update);
        }
    }

    private void processMessage(Message receivedMessage) {
        long chatId = receivedMessage.getChatId();

        CommandProcessor processor = commandProcessorConfig.getCommandProcessors().stream()
                .filter(commandProcessor -> commandProcessor.getCommandName().equals(receivedMessage.getText()))
                .findFirst()
                .orElse(null);

        SendMessage responseMessage;
        if (processor == null) {
            responseMessage = nonCommandMessageProcessor.process(receivedMessage);
        } else {
            responseMessage = processor.process(chatId, receivedMessage);
        }

        try {
            execute(responseMessage);
        } catch (TelegramApiException ex) {
            throw new RuntimeException(ex.getMessage());
        }

    }

    private void processCallbackQuery(Update update) {
        CallbackProcessor processor = callbackProcessorConfig.getCallbackProcessors().stream()
                .filter(callbackProcessor -> callbackProcessor.getProcessingCallbacks().stream()
                        .anyMatch(processingCallback -> processingCallback.equals(update.getCallbackQuery().getData())))
                .findFirst()
                .orElse(null);
        SendMessage responseMessage = new SendMessage();
        if (processor == null) {
            responseMessage.setChatId(update.getCallbackQuery().getMessage().getChatId());
            responseMessage.setText(ServiceMessageConstants.NO_CALLBACKS_FOUND);
        } else {
            responseMessage = processor.process(update.getCallbackQuery());
        }

        try {
            execute(responseMessage);
        } catch (TelegramApiException ex) {
            throw new RuntimeException(ex.getMessage());
        }
    }

    @Override
    public String getBotUsername() {
        return botConfig.getBotName();
    }

    @Override
    public String getBotToken() {
        return botConfig.getToken();
    }
}
